extern crate notify_rust;

use notify_rust::{Notification, NotificationHint as Hint, NotificationUrgency};
use std::{
    fs::{read_dir, read_to_string},
    io::{Error, ErrorKind, Result as IOResult},
    path::{Path, PathBuf},
    process::{Command, Stdio},
    time::Duration,
};

const INTERVAL: Duration = Duration::from_secs(20);
const POWER_SUPPLIES: &str = "/sys/class/power_supply";

#[derive(PartialEq, Eq)]
enum Power {
    Charging,
    Discharging,
}

struct Battery {
    power: Power,
    capacity: u8,
    name: String,
}

fn available_supplies(path: &Path) -> Vec<PathBuf> {
    read_dir(path)
        .unwrap()
        .map(|entry| entry.unwrap().path())
        .filter(|path| path.join("capacity").exists())
        .collect::<Vec<PathBuf>>()
}

fn supply_stats(path: &PathBuf) -> IOResult<Battery> {
    let status_path = path.as_path().join("status");
    let capacity_path = path.as_path().join("capacity");

    let status: String = read_to_string(&status_path)?.trim().into();
    let capacity: String = read_to_string(&capacity_path)?.trim().into();

    let power = match status.as_str() {
        "Charging" => Power::Charging,
        "Discharging" => Power::Discharging,
        _ => return Err(Error::new(ErrorKind::Other, "Unknown battery state.")),
    };

    let capacity = capacity.parse::<u8>().unwrap();

    let name = path.as_path().file_name().unwrap().to_str().unwrap().into();

    Ok(Battery {
        power,
        capacity,
        name,
    })
}

struct PercentageRange {
    start: u8,
    end: u8,
}

impl PercentageRange {
    fn in_range(&self, value: u8) -> bool {
        value >= self.start && value < self.end
    }
}

enum ScenarioAction {
    None,
    PowerOff,
    CancelPowerOff,
}

struct Scenario {
    power: Power,
    range: PercentageRange,
    icon: String,
    message: String,
    action: ScenarioAction,
}

#[cfg(all(unix))]
fn main() {
    let power_supplies = Path::new(POWER_SUPPLIES);

    if !power_supplies.exists() {
        panic!("The power supplies directory doesn't exist.");
    }

    let supplies = available_supplies(&power_supplies);

    // try to avoid start end intersections
    let scenarios: Box<Vec<Scenario>> = Box::new(vec![
        Scenario {
            power: Power::Discharging,
            range: PercentageRange { start: 0, end: 5 },
            icon: "battery-empty".into(),
            message: "can't hold out any longer!".into(),
            action: ScenarioAction::PowerOff,
        },
        Scenario {
            power: Power::Charging,
            range: PercentageRange { start: 0, end: 10 },
            icon: "battery-empty-charging".into(),
            message: "will live!".into(),
            action: ScenarioAction::CancelPowerOff,
        },
        Scenario {
            power: Power::Discharging,
            range: PercentageRange { start: 5, end: 10 },
            icon: "battery-caution".into(),
            message: "is running critically low!".into(),
            action: ScenarioAction::None,
        },
        Scenario {
            power: Power::Charging,
            range: PercentageRange {
                start: 97,
                end: 100,
            },
            icon: "battery-full-charging".into(),
            message: "is fully charged!".into(),
            action: ScenarioAction::None,
        },
    ]);

    let mut pending_shutdown = false;

    loop {
        let supply_stats = supplies
            .iter()
            .map(supply_stats)
            .filter_map(Result::ok)
            .collect::<Vec<Battery>>();

        let mut show_notification = true;

        for scenario in &(*scenarios) {
            for supply in &supply_stats {
                if supply.power != scenario.power {
                    continue;
                }

                if scenario.range.in_range(supply.capacity) {
                    match scenario.action {
                        ScenarioAction::PowerOff => {
                            if !pending_shutdown
                                && Command::new("shutdown")
                                    .stdout(Stdio::null())
                                    .stderr(Stdio::null())
                                    .spawn()
                                    .is_ok()
                            {
                                pending_shutdown = true;
                            } else {
                                show_notification = false;
                            }
                        }
                        ScenarioAction::CancelPowerOff => {
                            if pending_shutdown
                                && Command::new("shutdown")
                                    .arg("-c")
                                    .stdout(Stdio::null())
                                    .stderr(Stdio::null())
                                    .spawn()
                                    .is_ok()
                            {
                                pending_shutdown = false;
                            } else {
                                show_notification = false;
                            }
                        }
                        ScenarioAction::None => {
                            show_notification = true;
                        }
                    }

                    if show_notification {
                        Notification::new()
                            .appname("Battery Notifier")
                            .summary("Battery")
                            .hint(Hint::Category("battery".into()))
                            .urgency(NotificationUrgency::Critical)
                            .body(&format!("{} {}", supply.name, scenario.message))
                            .icon(&scenario.icon)
                            .show()
                            .unwrap();
                    }
                }
            }
        }

        std::thread::sleep(INTERVAL);
    }
}
